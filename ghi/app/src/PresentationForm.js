import React, { useEffect, useState } from 'react';

function PresentationForm() {

    const [conferences, setConferences] = useState([]);
    // Set the useState hook to store "name" in the component's state,
    // with a default initial value of an empty string.
    const [presenterName, setPresenterName] = useState('');
    const [presenterEmail, setPresenterEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [conference, setConference] = useState('');

    //Get data to populate the dropdown
    const fetchData = async () => {
      const url = 'http://localhost:8000/api/conferences/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);
      }
    }

    useEffect(() => {
      fetchData();
    }, []);

    const handleSubmit = async (event) => {
      event.preventDefault();
      //create an empty JSON Object, fill with python keys, JS values
      const data = {};
      data.presenter_name = presenterName;
      data.presenter_email = presenterEmail;
      data.company_name = companyName;
      data.title = title;
      data.synopsis = synopsis;
      data.conference = conference;

      //decide which URL to post to
      const presentationUrl = `http://localhost:8000/${conference}presentations/`;
      const fetchOptions = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const attendeeResponse = await fetch(presentationUrl, fetchOptions);
      if (attendeeResponse.ok) {
        //after posting the request, these correspond with the value on the input tags to clear the data in the form fields
        setPresenterName('');
        setPresenterEmail('');
        setCompanyName('');
        setTitle('');
        setSynopsis('');
        setConference('');
      }
    }

    const handleChangePresenterName = (event) => {
      const value = event.target.value;
      setPresenterName(value);
    }
    // Create the handleNameChange method to take what the user inputs
    // into the form and store it in the state's "name" variable.
    const handleChangePresenterEmail = (event) => {
      const value = event.target.value;
      setPresenterEmail(value);
    }

    const handleChangeCompanyName = (event) => {
      const value = event.target.value;
      setCompanyName(value);
    }

    const handleChangeTitle = (event) => {
        const value = event.target.value;
        setTitle(value);
      }


      const handleChangeSynopsis = (event) => {
        const value = event.target.value;
        setSynopsis(value);
      }


      const handleChangeConference = (event) => {
        const value = event.target.value;
        setConference(value);
      }





    return (

        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new presentation</h1>
                <form onSubmit={handleSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                    <input value={presenterName} onChange={handleChangePresenterName} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                    <label htmlFor="presenter_name">Presenter name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={presenterEmail} onChange={handleChangePresenterEmail} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                    <label htmlFor="presenter_email">Presenter email</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={companyName} onChange={handleChangeCompanyName}placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                    <label htmlFor="company_name">Company name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={title} onChange={handleChangeTitle} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                    <label htmlFor="title">Title</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="synopsis">Synopsis</label>
                    <textarea value={synopsis} onChange={handleChangeSynopsis} className="form-control" id="synopsis" rows="3" name="synopsis" className="form-control"></textarea>
                </div>
                <div className="mb-3">
                    <select value={conference} onChange={handleChangeConference} required name="conference" id="conference" className="form-select">
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => {
                    return (
                        <option key={conference.href} value={conference.href}>
                            {conference.name}
                        </option>
                    );
                  })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>

    );
  }

  export default PresentationForm;
