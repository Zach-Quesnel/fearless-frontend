function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
    return `

            <div class="col">
                <div class = "card shadow p-3 mb-5 bg-body-tertiary rounded">
                    <img src="${pictureUrl}" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">${name}</h5>
                        <h6 class="card-subtitle mb-2 text-body-secondary">${locationName}</h6>
                        <p class="card-text">${description}</p>
                    </div>
                    <div class="card-footer">
                    <small class="text-body-secondary">${startDate} - ${endDate}</small>
                    </div>
                </div>
            </div>


    `;
    }



window.addEventListener('DOMContentLoaded', async () => {


    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error('Response not ok');
        }   else {

        const data = await response.json(); //receives a javascript object with requested data
        //console.log(data);

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const startDate = new Date(details.conference.starts).toLocaleDateString();
                const endDate = new Date(details.conference.ends).toLocaleDateString();
                const locationName = details.conference.location.name
                const html = createCard(name, description, pictureUrl, startDate, endDate, locationName);

                //console.log(html);
                const column = document.querySelector('.row');
                column.innerHTML += html;
            }

        //const conference = data.conferences[0]; //selects the conference name from the data object, not needed in for loop
        //console.log(conference);
        //const nameTag = document.querySelector('.card-title'); //selects the html tag with class .card-title
        //nameTag.innerHTML = conference.name; //sets the conference name to the content of the html tag

        //const detailUrl = `http://localhost:8000${conference.href}`;
        //const detailResponse = await fetch(detailUrl);
        //if (detailResponse.ok) {
          //const details = await detailResponse.json(); //use API request in insomnia to see the data structure for accessing details
          //console.log(details);

          //const descriptionDict = details.conference;
          //const descriptionTag = document.querySelector('.card-text');
          //descriptionTag.innerHTML = descriptionDict.description;

          //const imageTag = document.querySelector('.card-img-top');
          //imageTag.src = details.conference.location.picture_url;
        }

      }
    } catch (error) {
        console.error('error', error);
    }

  });
